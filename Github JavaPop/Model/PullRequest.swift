//
//  PullRequest.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 04/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit
import ObjectMapper

class PullRequest: Mappable {
    // MARK: - Properties -
    var authorName: String?
    var authorAvatarUrl: String?
    var title: String?
    var date: String?
    var body: String?
    var htmlUrl: String?
    
    // MARK: - Mapping -
    required init?(map: Map){ }
    
    func mapping(map: Map) {
        authorName      <- map["user.login"]
        authorAvatarUrl <- map["user.avatar_url"]
        title           <- map["title"]
        date            <- map["created_at"]
        body            <- map["body"]
        htmlUrl         <- map["html_url"]
    }
}
