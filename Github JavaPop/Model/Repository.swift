//
//  Repository.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 04/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: Mappable {
    // MARK: - Properties -
    var name: String?
    var description: String?
    var authorName: String?
    var authorAvatarUrl: String?
    var authorPhoto: UIImage?
    var starsNumber: Int?
    var forksNumber: Int?
    var pullRequestsUrl: String?
    
    // MARK: - Mapping -
    required init?(map: Map){ }
    
    func mapping(map: Map) {
        name            <- map["name"]
        description     <- map["description"]
        authorName      <- map["owner.login"]
        authorAvatarUrl <- map["owner.avatar_url"]
        starsNumber     <- map["stargazers_count"]
        forksNumber     <- map["forks_count"]
        pullRequestsUrl <- (map["pulls_url"], TransformOf<String, String>(fromJSON: { $0?.replacingOccurrences(of: Constants.pullRequestUrlNumberPlaceholder, with: "") }, toJSON: { $0 }))
    }
}
