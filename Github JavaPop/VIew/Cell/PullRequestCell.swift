//
//  PullRequestCell.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 06/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit
import AlamofireImage

class PullRequestCell: UITableViewCell {
    // MARK: - IBOutlet -
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UITextView!
    @IBOutlet weak var authorPhoto: UIImageView!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var date: UILabel!
    
    // MARK: - View Life Cycle -
    override func layoutSubviews() {
        super.layoutSubviews()
        
        body.textContainer.lineFragmentPadding = 0
        body.textContainerInset = .zero
        body.textContainer.maximumNumberOfLines = 2
        body.textContainer.lineBreakMode = .byTruncatingTail
    }
    
    // MARK: - Public -
    func fillCell(withPullRequest pullRequest: PullRequest) {
        title.text = pullRequest.title
        body.text = pullRequest.body
        authorName.text = pullRequest.authorName
        
        if let date = pullRequest.date {
            self.date.text = String(date.prefix(10))
        }
        
        if let avatarUrl = pullRequest.authorAvatarUrl {
            if let url = URL(string: avatarUrl) {
                let avatarPlaceholderImage = UIImage(named: "avatarPlaceholder")
                
                authorPhoto.af_setImage(withURL: url, placeholderImage: avatarPlaceholderImage ?? nil)
                authorPhoto.layer.cornerRadius = authorPhoto.frame.size.width / 2;
                authorPhoto.clipsToBounds = true
            }
        }
    }
}
