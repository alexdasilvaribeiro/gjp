//
//  RepositoryCell.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 05/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit
import AlamofireImage

class RepositoryCell: UITableViewCell {
    // MARK: - IBOutlet -
    @IBOutlet weak var repositoryName: UILabel!
    @IBOutlet weak var repositoryDescription: UITextView!
    @IBOutlet weak var authorPhoto: UIImageView!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var forksNumber: UILabel!
    @IBOutlet weak var starsNumber: UILabel!
    
    // MARK: - View Life Cycle -
    override func layoutSubviews() {
        super.layoutSubviews()
        
        repositoryDescription.textContainer.lineFragmentPadding = 0
        repositoryDescription.textContainerInset = .zero
        repositoryDescription.textContainer.maximumNumberOfLines = 2
        repositoryDescription.textContainer.lineBreakMode = .byTruncatingTail
    }
    
    // MARK: - Public -
    func fillCell(withRepository repository: Repository) {
        repositoryName.text = repository.name
        repositoryDescription.text = repository.description
        forksNumber.text = String(describing: repository.forksNumber ?? 0)
        starsNumber.text = String(describing: repository.starsNumber ?? 0)
        
        if let avatarUrl = repository.authorAvatarUrl {
            if let url = URL(string: avatarUrl) {
                let avatarPlaceholderImage = UIImage(named: "avatarPlaceholder")
                
                authorPhoto.af_setImage(withURL: url, placeholderImage: avatarPlaceholderImage ?? nil)
                authorPhoto.layer.cornerRadius = authorPhoto.frame.size.width / 2;
                authorPhoto.clipsToBounds = true
            }
        }
        
        authorName.text = repository.authorName
    }
}
