//
//  Constants.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 05/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

class Constants {
    static let repositoriesScreenTitle = "Github JavaPop"
    static let repositoryCellIdentifier = "repositoryCell"
    static let pullRequestCellIdentifier = "pullRequestCell"
    static let pullRequestUrlNumberPlaceholder = "{/number}"
    static let pullRequestSegueIdentifier = "pullRequestSegue"
}
