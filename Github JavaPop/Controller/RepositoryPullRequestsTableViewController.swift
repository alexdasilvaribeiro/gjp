//
//  RepositoryPullRequestsTableViewController.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 06/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit

class RepositoryPullRequestsTableViewController: UITableViewController {
    // MARK: - Properties -
    var repository: Repository?
    var pullRequests = [PullRequest]()
    
    // MARK: - View Life Cycle -
    override func viewDidLoad() {
        if let repository = repository {
            getPullRequests()
            self.tableView.startSpinner()
            navigationItem.title = repository.name
        }
    }
    
    // MARK: - TableView Data Source -
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count
    }
    
    // MARK: - TableView Delegate -
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.pullRequestCellIdentifier, for: indexPath) as? PullRequestCell {
            
            cell.fillCell(withPullRequest: pullRequests[indexPath.row])
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let pullRequestHtmlUrl = pullRequests[indexPath.row].htmlUrl {
            if let url = URL(string: pullRequestHtmlUrl) {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    // MARK: - Private
    private func getPullRequests() {
        GithubAPI.getAllPullRequests(forRepository: repository) { pullRequests in
            if let pullRequests = pullRequests {
                self.pullRequests = pullRequests
                self.tableView.reloadData()
                self.tableView.stopSpinner()
            }
        }
    }
}
