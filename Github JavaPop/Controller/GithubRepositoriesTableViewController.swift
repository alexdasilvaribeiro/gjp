//
//  GithubRepositoriesTableViewController.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 04/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit

class GithubRepositoriesTableViewController: UITableViewController {
    // MARK: - Properties -
    var repositories = [Repository]()
    var selectedRepository: Repository?
    var page = 1
    
    // MARK: - View Life Cycle -
    override func viewDidLoad() {
        getRepositories()
        self.tableView.startSpinner()
        navigationItem.title = Constants.repositoriesScreenTitle
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.pullRequestSegueIdentifier {
            if let destination = segue.destination as? RepositoryPullRequestsTableViewController {
                destination.repository = selectedRepository
            }
        }
    }
    
    // MARK: - TableView Data Source -
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    // MARK: - TableView Delegate -
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.repositoryCellIdentifier, for: indexPath) as? RepositoryCell {
            
            cell.fillCell(withRepository: repositories[indexPath.row])
            
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == repositories.count - 1 {
            page += 1
            getRepositories()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRepository = repositories[indexPath.row]
        performSegue(withIdentifier: Constants.pullRequestSegueIdentifier, sender: self)
    }
    
    // MARK: - Private
    private func getRepositories() {
        GithubAPI.getAllRepositories(forLanguage: .java, page: page) { repositories in
            if let repositories = repositories {
                self.repositories.append(contentsOf: repositories)
                self.tableView.reloadData()
            }
        }
    }
}
