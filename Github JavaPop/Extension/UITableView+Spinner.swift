//
//  UITableView+Spinner.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 06/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import UIKit

extension UITableView {
    // MARK: - Public -
    func startSpinner() {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        spinner.startAnimating()
        spinner.frame = CGRect(x: 0, y: 0, width: frame.width, height: 44)
        tableFooterView = spinner
    }
    
    func stopSpinner() {
        tableFooterView = nil
    }
}
