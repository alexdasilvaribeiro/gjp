//
//  GithubAPI.swift
//  Github JavaPop
//
//  Created by Alex da Silva Ribeiro on 04/10/17.
//  Copyright © 2017 Alex da Silva Ribeiro. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

class GithubAPI {
    // MARK: - Enum -
    enum Language: String {
        case java = "Java"
    }
    
    // MARK: - Public -
    class func getAllRepositories(forLanguage language: Language, page: Int = 1, completion: @escaping(([Repository]?) -> ())) {
        let url = "https://api.github.com/search/repositories?q=language:\(language)&sort=stars&page=\(page)"
        
        Alamofire.request(url).responseArray(queue: nil, keyPath: "items", context: nil) { (response: DataResponse<[Repository]>) in
            if let repositories = response.result.value {
                completion(repositories)
            } else {
                completion(nil)
            }
        }
    }
    
    class func getAllPullRequests(forRepository repository: Repository?, completion: @escaping(([PullRequest]?) -> ())) {
        if let url = repository?.pullRequestsUrl {
            Alamofire.request(url).responseArray(completionHandler: { (response: DataResponse<[PullRequest]>) in
                if let pullRequests = response.result.value {
                    completion(pullRequests)
                } else {
                    completion(nil)
                }
            })
        } else {
            completion(nil)
        }
    }
}
