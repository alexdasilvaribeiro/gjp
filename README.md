# Github JavaPop

### Dependency Manager
* [Cocoapods](https://cocoapods.org/) - CocoaPods is a dependency manager for Swift and Objective-C Cocoa projects. It has over 37 thousand libraries and is used in over 2.6 million apps. CocoaPods can help you scale your projects elegantly.
### Dependencies
* [Alamofire](https://github.com/Alamofire/Alamofire) - Elegant Networking in Swift.
* [AlamofireImage](https://github.com/Alamofire/AlamofireImage) - AlamofireImage is an image component library for Alamofire.
* [AlamofireObjectMapper](https://github.com/tristanhimmelman/AlamofireObjectMapper) - An extension to Alamofire which automatically converts JSON response data into swift objects using ObjectMapper.

--------
###### AlexSilR
![Powered By Hop](https://cldup.com/aXBRvO85mb.png)
